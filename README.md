This project is being developed to automate tennis scoring system. In real, tennis scoring system is little confusing to newbies. 

So, we are working on this project to make scoring easy for new tennis players to make their training experience delightful and easy. Everybody is welcome to contribute. 

Before contributing, please read carefully, how tennis scoring system works here: https://supertennisracquet.com/how-does-tennis-score-work/